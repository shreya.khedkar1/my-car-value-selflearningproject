import { Report } from '../reports/reports.entity';
import {
  AfterInsert,
  AfterRemove,
  AfterUpdate,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column({ default: true })
  admin: boolean;

  @OneToMany(() => Report, (report) => report.user)
  reports: Report[];

  @AfterInsert()
  logInsert() {
    console.log('Inserted a new user with id : ', this.id);
  }

  @AfterUpdate()
  logUpdate() {
    console.log('Updated a user with id : ', this.id);
  }

  @AfterRemove()
  logRemove() {
    console.log('Removed a user with id : ', this.id);
  }
}
