import { Test } from '@nestjs/testing';
import { UsersService } from './users.service';
import { AuthService } from './auth.service';
import { User } from './users.entity';

let service: AuthService;
let fakeUsersService: Partial<UsersService>;

beforeEach(async () => {
  fakeUsersService = {
    find: () => Promise.resolve([]),
    create: (email: string, password: string) =>
      Promise.resolve({ id: 1, email, password } as User),
  };
  const module = await Test.createTestingModule({
    providers: [
      AuthService,
      {
        provide: UsersService,
        useValue: fakeUsersService,
      },
    ],
  }).compile();

  service = module.get(AuthService);

  it('can create an instance of auth service', async () => {
    expect(service).toBeDefined();
  });

  it('creates a new user with a salted and hashed password', async () => {
    const user = await service.signup('some@some.com', 'some');

    expect(user.password).not.toEqual('some');
    const [salt, hash] = user.password.split('.');
    expect(salt).toBeDefined();
    expect(hash).toBeDefined();
  });

  it('thows an error if user signs up with email that is in use', async (done) => {
    fakeUsersService.find = () =>
      Promise.resolve([{ id: 1, email: 'a', password: '1' } as User]);
    try {
      await service.signup('some@some.com', 'some');
    } catch (err) {
      done();
    }
  });

  it('throws if signing is called with an unused email id', async (done) => {
    try {
      await service.signin('some@some.com', 'some');
    } catch (err) {
      done();
    }
  });

  it('throws if an invalid password provided', async (done) => {
    fakeUsersService.find = () =>
      Promise.resolve([{ email: 'some@some.com', password: 'some' } as User]);
    try {
      await service.signin('email@e.com', 'pass');
    } catch (err) {
      done();
    }
  });

  it('returns a user if correct password is provided', async () => {
    fakeUsersService.find = () =>
      Promise.resolve([
        {
          email: 'test@test.com',
          password:
            '1088c8fdb1c75d3c.5d82598fbdfe5072c7be27baf98145308fcbbe7d7e57fda9bbae1602e13bcb44',
        } as User,
      ]);
    const user = await service.signin('email@e.com', '12345');
    expect(user).toBeDefined();
  });
});
